// dependencies
#include <mpi.h>
#include <fftw3.h>
#include <fftw3-mpi.h>
#include <hdf5.h>
#include <gsl/gsl_rng.h>

#define PI 2.0*acos(0.0)


//---------- State Stuff ----------//

// Define the state structure, which is what we will operate on
typedef struct
{
  // The k magnitude field
  double* mag_k;
  // The C_2(k) field
  double* C2_k;
  // The n and c field, and their transforms
  double* n;
  fftw_complex* nk;
  double* c;
  fftw_complex* ck;
  // The various update fields and their transforms
  double* NL_n;
  fftw_complex* NL_nk;
  double* NL_c;
  fftw_complex* NL_ck;
  double* Corr_n;
  fftw_complex* Corr_nk;
  double* Corr_c;
  fftw_complex* Corr_ck;
  double* expkA;
  fftw_complex* expkA_k;
  // double* expkB;
  // fftw_complex* expkB_k;
  fftw_complex* xi_n_k;
  fftw_complex* xi_c_k;

  // Some model or simulation parameters
  double dx;    // spatial resolution
  double dt;    // timestep
  double eta;   // free energy fitting parameter
  double chi;   // free energy fitting parameter
  double w;     // strength of the mixing term
  double n0;    // the average density
  double eps0;   // enthalpy of mixing
  double eps1;
  double eps;
  double Wc;    // concentration grain boundary penalty
  double k_A;    // alpha phase wave vector
  double sig_kA; // alpha phase peak width
  double H0;    // height of the orientation bias
  double gamma_A;  // width of the first peak of the orientation bias
  // double k_B;    // beta phase wave vector
  // double sig_kB; // beta phase peak width
  double a_c;   // susceptibily of melting temperature
  double T;     // scaled simulation temperature
  double T0;    // temperature scaling parameter
  double TM;    // melting temperature parameter
  double Tc;    // critical temperature of the liquid
  double M_n;    // density mobility
  double xi_n;  // strength fo the total density noise
  double M_c;    // concentration mobility
  double xi_c;  // strength of the concentration noise
  int SzLq_x;
  int SzLq_y;
  int SzLq_z;
  double misOrr_x;    // the misorrientation of the grains.
  double cosOrr_x;    // cosine of the misorrientation, for rotation.
  double sinOrr_x;    // sine of the misorrienation for rotation.
  double misOrr_y;    // the misorrientation of the grains.
  double cosOrr_y;    // cosine of the misorrientation, for rotation.
  double sinOrr_y;    // sine of the misorrienation for rotation.
  double misOrr_z;    // the misorrientation of the grains.
  double cosOrr_z;    // cosine of the misorrientation, for rotation.
  double sinOrr_z;    // sine of the misorrienation for rotation.
  double t;     // current physical time
  int step;     // current time step

  ptrdiff_t Nx;              // size of the first dimension of the grid
  ptrdiff_t Ny;             // size of the second dimension of the grid
  ptrdiff_t Nz;             // sizee of the third dimension of the grid
  ptrdiff_t local_n0;       // local endpoint of a processor
  ptrdiff_t local_0_start;  // real start of a processor
  ptrdiff_t local_n1;       // Used for transposed fftw
  ptrdiff_t local_1_start;  // Same.

  // The plans for fast fourier transforms
  fftw_plan pfft;
  fftw_plan ipfft;

  // The RNG
  gsl_rng* rng;

} state;

state* create_state(int Nx, int Ny, int Nz, double dx, double dt,
                    double eta, double chi,
                    double w, double Wc, double T, double T0, double TM, double Tc,
                    double a_c, double eps0, double eps1,
                    double k_A, double sig_kA, double H0, double gamma_A,
                    double Mn, double xi_n, double Mc, double xi_c,
                    double t, int step, double n0, double c0,
                    double misOrr_x, double misOrr_y, double misOrr_z,
                    int SzLq_x, int SzLq_y, int SzLq_z);

void destroy_state(state* s);

void seed_state_bcc(state* s, double kl, double Amp, int posx, int posy, int posz, int r,
                double orr_x, double orr_y, double orr_z, double seed_c, double seed_n);

void seed_slab_state_bcc(state* s, double kl, double Amp, int dim, int pos, int r,
                double orr_x, double orr_y, double orr_z, double seed_c, double seed_n);

void seed_slab_bicrystal_bcc(state* s, int dim, double c_shift);
void seed_slab_bicrystal_bcc2(state* s, int dim, double c_shift);


void seed_slab_multicrystal_bcc(state* s, int dim, int number, double c_shift);

void seed_slab_bcc_L(state* s, int dim, double c_shift);
void seed_c_gradient(state* s, int dim, double c_shift);


//---------- Dynamics Stuff ----------//
double k2(int i, int j, int k, int Nx, int Ny, int Nz, double dx);
double k_x(int j, int Nx, double dx);
double k_y(int i, int Ny, double dx);
double k_z(int k, int Nz, double dx);
double Intp(state* s, double c);
double DcIntp(state* s, double c);

void Calc_mag_k(state* s);
void Calc_Density_transforms(state* s);
void Calc_Corr(state* s);
void Calc_NL(state* s);
void Calc_Noise(state* s);
void step(state* s);
void QD_step(state* s);

void Ramp_T(state* s, double when, double duration, double rate);
void Prep_Simulation(state* s);

//---------- Error handling ----------//

void my_error (const char* error_string);


//---------- Input/Output Stuff ----------//
void mpi_print (const char *str);
hid_t io_init_new_file (const char *filename);
hid_t io_init_from_file (const char *filename);
herr_t io_finalize (hid_t file_id);
herr_t save_state (state* s, hid_t file_id);
herr_t save_state_verbose (state* s, hid_t file_id);
state* load_state (hid_t file_id, const char *datafile);
state* load_grain_boundary (const char *datafile1,
                            const char *datafile2);
herr_t op_func (hid_t loc_id, const char *name, const H5L_info_t *info,
                void *operator_data);
state* load_last_saved_state(hid_t      file_id);
state* new_state_from_file (const char *filename);


//---------- FFTW Wisdom Stuff ----------//
void load_wisdom(int argc, char** argv);
void save_wisdom(void);
