/*
 * This file is modified from Diffusion Equation MPI
 * Nathan Smith (c) 2016
 *
 * for use in Isobaric PFC
 * Matthew Frick (c) 2019
 * This file impliments I/0 using HDF5
 */


#include <mpi.h>        // MPI runtime
#include <hdf5.h>       // HDF5 header
#include <stdlib.h>     // malloc etc
#include <unistd.h>     // access() to check if file exists
#include <stdbool.h>    // Booleans
#include <assert.h>     // Assertions
#include <string.h>     // String manipulation sprintf etc...
#include <math.h>       // Needed for sqrt

#include "bcc_nc_header.h"    // local libbinary header

#define FAIL -1
#define LEN 8

const int io_verbose = true;

int save_to_load;

void mpi_print (const char *str)
{
    int rank;
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    if (rank == 0) printf("%s\n", str);
    MPI_Barrier (MPI_COMM_WORLD);
}

hid_t io_init_from_file (const char *filename)
{
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;
    hid_t plist_id;
    hid_t file_id;
    herr_t status;

    plist_id = H5Pcreate (H5P_FILE_ACCESS);

    H5Pset_fapl_mpio (plist_id, comm, info);

    file_id = H5Fopen (filename, H5F_ACC_RDWR, plist_id);

    status = H5Pclose (plist_id);
    assert (status != FAIL);

    return file_id;
}


hid_t io_init_new_file (const char *filename)
{
    /*
    *  Create a file to save data to for this session
    */
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;
    hid_t plist_id;     /* Property list id */
    hid_t file_id;      /* File id */
    herr_t err;         /* Error status */

    plist_id = H5Pcreate (H5P_FILE_ACCESS);

    H5Pset_fapl_mpio (plist_id, comm, info);

    file_id = H5Fcreate (filename,
                         H5F_ACC_TRUNC,
                         H5P_DEFAULT,
                         plist_id );

    err = H5Pclose (plist_id);
    assert (err != FAIL);
    return file_id;
}

herr_t io_finalize (hid_t file_id)
{
    return H5Fclose(file_id);
}

herr_t write_array_dataset (const char *name,
                            hid_t       group_id,
                            double     *arr,
                            state      *s)
{
     hid_t dset_id, dataspace;
     hid_t memspace, plist_id;
     herr_t status;
     hsize_t size[3], count[3], offset[3];

     /* Describe the shape of the array */
     size[0] = s->Nx;
     size[1] = s->Ny;
     size[2] = s->Nz;
     dataspace = H5Screate_simple(3, size, NULL);


      /* Create Dataset */
     dset_id = H5Dcreate(group_id,
                         name,
                         H5T_NATIVE_DOUBLE,
                         dataspace,
                         H5P_DEFAULT,
                         H5P_DEFAULT,
                         H5P_DEFAULT);

     /* Describe memory shape, property list and data shape */
     count[0] = 1;
     count[1] = 1;
     count[2] = s->Nz;
     // offset[1] = 0;
     offset[2] = 0;
     memspace = H5Screate_simple (3, count, NULL);

     /* Set up some of the MPI things */
     plist_id = H5Pcreate (H5P_DATASET_XFER);
     H5Pset_dxpl_mpio (plist_id, H5FD_MPIO_INDEPENDENT);

     /* Write data row by row in slabs */
     for (int i = 0; i < s->local_n0; i++)
     {
         offset[0] = s->local_0_start + i;

         for (int j = 0; j < s->Ny; j++)
         {
           offset[1] = j;
           status = H5Sselect_hyperslab (dataspace,
                                         H5S_SELECT_SET,
                                         offset,
                                         NULL,
                                         count,
                                         NULL);

           status = H5Dwrite (dset_id,
                              H5T_NATIVE_DOUBLE,
                              memspace,
                              dataspace,
                              plist_id,
                              arr + 2*((s->Nz>>1)+1)*j + 2*((s->Nz>>1)+1)*s->Ny*i);
          }
     }

     /* Close everything you opened */
     status = H5Pclose (plist_id);
     status = H5Sclose (memspace);
     status = H5Dclose (dset_id);
     status = H5Sclose (dataspace);

     return status;
}

herr_t write_Rek_array_dataset (const char *name,
                            hid_t       group_id,
                            fftw_complex     *arr,
                            state      *s)
{
     hid_t dset_id, dataspace;
     hid_t memspace, plist_id;
     herr_t status;
     hsize_t size[3], count[3], offset[3];

     /* Describe the shape of the array */
     size[0] = s->Ny;
     size[1] = s->Nx;
     size[2] = ((s->Nz>>1)+1);

     int ijk;
     int arrsize = s->Nx*s->Ny*((s->Nz>>1)+1);
     double arr2[arrsize];
     for (int i = 0; i < s->local_n1; i++)
     {
       for (int j = 0; j < s->Nx; j++)
       {
         for (int k = 0; k < ((s->Nz>>1)+1); k++)
         {
           ijk = k + ((s->Nz>>1)+1)*(j + s->Nx*i);
           arr2[ijk] = arr[ijk][0];
         }
       }
     }

     dataspace = H5Screate_simple(3, size, NULL);


      /* Create Dataset */
     dset_id = H5Dcreate(group_id,
                         name,
                         H5T_NATIVE_DOUBLE,
                         dataspace,
                         H5P_DEFAULT,
                         H5P_DEFAULT,
                         H5P_DEFAULT);

     /* Describe memory shape, property list and data shape */
     count[0] = 1;
     count[1] = 1;
     count[2] = ((s->Nz>>1)+1);
     offset[2] = 0;
     memspace = H5Screate_simple (3, count, NULL);

     /* Set up some of the MPI things */
     plist_id = H5Pcreate (H5P_DATASET_XFER);
     H5Pset_dxpl_mpio (plist_id, H5FD_MPIO_INDEPENDENT);

     /* Write data row by row in slabs */
     for (int i = 0; i < s->local_n1; i++)
     {
         offset[0] = s->local_1_start + i;
         for (int j = 0; j < s->Nx; j++)
         {
           offset[1] = j;

           status = H5Sselect_hyperslab (dataspace,
                                         H5S_SELECT_SET,
                                         offset,
                                         NULL,
                                         count,
                                         NULL);

           status = H5Dwrite (dset_id,
                              H5T_NATIVE_DOUBLE,
                              memspace,
                              dataspace,
                              plist_id,
                              arr2 + ((s->Nz>>1)+1)*j + ((s->Nz>>1)+1)*s->Nx*i);
           }
     }

     /* Close everything you opened */
     status = H5Pclose (plist_id);
     status = H5Sclose (memspace);
     status = H5Dclose (dset_id);
     status = H5Sclose (dataspace);

     return status;
}

herr_t write_Re_ksize_array_dataset (const char *name,
                            hid_t       group_id,
                            double     *arr,
                            state      *s)
{
     hid_t dset_id, dataspace;
     hid_t memspace, plist_id;
     herr_t status;
     hsize_t size[3], count[3], offset[3];

     /* Describe the shape of the array */
     size[0] = s->Ny;
     size[1] = s->Nx;
     size[2] = ((s->Nz>>1)+1);

     int ijk;

     dataspace = H5Screate_simple(3, size, NULL);


      /* Create Dataset */
     dset_id = H5Dcreate(group_id,
                         name,
                         H5T_NATIVE_DOUBLE,
                         dataspace,
                         H5P_DEFAULT,
                         H5P_DEFAULT,
                         H5P_DEFAULT);

     /* Describe memory shape, property list and data shape */
     count[0] = 1;
     count[1] = 1;
     count[2] = ((s->Nz>>1)+1);
     offset[2] = 0;
     memspace = H5Screate_simple (3, count, NULL);

     /* Set up some of the MPI things */
     plist_id = H5Pcreate (H5P_DATASET_XFER);
     H5Pset_dxpl_mpio (plist_id, H5FD_MPIO_INDEPENDENT);

     /* Write data row by row in slabs */
     for (int i = 0; i < s->local_n1; i++)
     {
         offset[0] = s->local_1_start + i;
         for (int j = 0; j < s->Nx; j++)
         {
           offset[1] = j;

           status = H5Sselect_hyperslab (dataspace,
                                         H5S_SELECT_SET,
                                         offset,
                                         NULL,
                                         count,
                                         NULL);

           status = H5Dwrite (dset_id,
                              H5T_NATIVE_DOUBLE,
                              memspace,
                              dataspace,
                              plist_id,
                              arr + ((s->Nz>>1)+1)*j + ((s->Nz>>1)+1)*s->Nx*i);
           }
     }

     /* Close everything you opened */
     status = H5Pclose (plist_id);
     status = H5Sclose (memspace);
     status = H5Dclose (dset_id);
     status = H5Sclose (dataspace);

     return status;
}

herr_t write_Imk_array_dataset (const char *name,
                            hid_t       group_id,
                            fftw_complex     *arr,
                            state      *s)
{
  hid_t dset_id, dataspace;
  hid_t memspace, plist_id;
  herr_t status;
  hsize_t size[3], count[3], offset[3];

  /* Describe the shape of the array */
  size[0] = s->Ny;
  size[1] = s->Nx;
  size[2] = ((s->Nz>>1)+1);

  int ijk;
  double arr2[size[0]*size[1]*size[2]];
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx; j++)
    {
      for (int k = 0; k < ((s->Nz>>1)+1); k++)
      {
        ijk = k + ((s->Nz>>1)+1)*(j + s->Nx*i);
        arr2[ijk] = arr[ijk][1];
      }
    }
  }

  dataspace = H5Screate_simple(3, size, NULL);


   /* Create Dataset */
  dset_id = H5Dcreate(group_id,
                      name,
                      H5T_NATIVE_DOUBLE,
                      dataspace,
                      H5P_DEFAULT,
                      H5P_DEFAULT,
                      H5P_DEFAULT);

  /* Describe memory shape, property list and data shape */
  count[0] = 1;
  count[1] = 1;
  count[2] = ((s->Nz>>1)+1);
  offset[2] = 0;
  memspace = H5Screate_simple (3, count, NULL);

  /* Set up some of the MPI things */
  plist_id = H5Pcreate (H5P_DATASET_XFER);
  H5Pset_dxpl_mpio (plist_id, H5FD_MPIO_INDEPENDENT);

  /* Write data row by row in slabs */
  for (int i = 0; i < s->local_n1; i++)
  {
      offset[0] = s->local_1_start + i;
      for (int j = 0; j < s->Nx; j++)
      {
        offset[1] = j;

        status = H5Sselect_hyperslab (dataspace,
                                      H5S_SELECT_SET,
                                      offset,
                                      NULL,
                                      count,
                                      NULL);

        status = H5Dwrite (dset_id,
                           H5T_NATIVE_DOUBLE,
                           memspace,
                           dataspace,
                           plist_id,
                           arr2 + ((s->Nz>>1)+1)*j + ((s->Nz>>1)+1)*s->Nx*i);
        }
  }

  /* Close everything you opened */
  status = H5Pclose (plist_id);
  status = H5Sclose (memspace);
  status = H5Dclose (dset_id);
  status = H5Sclose (dataspace);

  return status;
}

herr_t read_array_dataset (const char *name,
                           hid_t       group_id,
                           double     *arr,
                           state      *s)
{
    hid_t dset_id, dataspace;
    hid_t memspace, plist_id;
    herr_t status;
    hsize_t count[3], offset[3];

    /* Open Dataset */
    dset_id = H5Dopen1 (group_id, name);

    /* Describe memory shape, property list and data shape */
    count[0] = 1;
    count[1] = 1;
    count[2] = s->Nz;
    offset[2] = 0;
    memspace = H5Screate_simple (2, count, NULL);

    /* Set up some of the MPI things */
    dataspace = H5Dget_space (dset_id);
    plist_id = H5Pcreate (H5P_DATASET_XFER);
    H5Pset_dxpl_mpio (plist_id, H5FD_MPIO_INDEPENDENT);

    /* Write data row by row in slabs */
    for (int i = 0; i < s->local_n0; i++)
    {
        offset[0] = s->local_0_start + i;

        for (int j = 0; j < s->Ny; j++)
        {
          offset[1] = j;
          status = H5Sselect_hyperslab (dataspace,
                                        H5S_SELECT_SET,
                                        offset,
                                        NULL,
                                        count,
                                        NULL);

          status = H5Dread (dset_id,
                            H5T_NATIVE_DOUBLE,
                            memspace,
                            dataspace,
                            plist_id,
                            arr + 2*((s->Nz>>1)+1)*j + 2*((s->Nz>>1)+1)*s->Ny*i);
        }
    }

    /* Close everything you opened */
    status = H5Pclose (plist_id);
    status = H5Sclose (memspace);
    status = H5Dclose (dset_id);
    status = H5Sclose (dataspace);

    return status;
}

herr_t write_double_attribute (const char *name,
                               hid_t       group_id,
                               double     *value)
{
     hsize_t size = 1;
     herr_t status;
     hid_t attr_id, dataspace;

     dataspace = H5Screate_simple(1, &size, NULL);
     attr_id = H5Acreate2 (group_id,
                           name,
                           H5T_NATIVE_DOUBLE,
                           dataspace,
                           H5P_DEFAULT,
                           H5P_DEFAULT);

     status = H5Awrite (attr_id, H5T_NATIVE_DOUBLE, value);
     status = H5Aclose (attr_id);

     return status;
}

herr_t read_double_attribute (const char *name,
                              hid_t       group_id,
                              double     *value)
{
    /* Read integer attribute from dataset 'group_id' */
    hid_t attr_id;
    herr_t status;

    attr_id = H5Aopen (group_id, name, H5P_DEFAULT);
    status = H5Aread (attr_id, H5T_NATIVE_DOUBLE, value);

    status = H5Aclose (attr_id);
    return status;
}

herr_t write_int_attribute (const char *name,
                            hid_t       group_id,
                            int        *value)
{
     hsize_t size = 1;
     herr_t status;
     hid_t attr_id, dataspace;

     dataspace = H5Screate_simple (1, &size, NULL);
     attr_id = H5Acreate2 (group_id,
                           name,
                           H5T_NATIVE_INT,
                           dataspace,
                           H5P_DEFAULT,
                           H5P_DEFAULT);

     status = H5Awrite (attr_id, H5T_NATIVE_INT, value);
     status = H5Aclose (attr_id);

     return status;
}

herr_t read_int_attribute (const char *name,
                           hid_t       group_id,
                           int        *value)
{
    /* Read integer attribute from dataset 'group_id' */
    hid_t attr_id;
    herr_t status;

    attr_id = H5Aopen (group_id, name, H5P_DEFAULT);
    status = H5Aread (attr_id, H5T_NATIVE_INT, value);

    status = H5Aclose (attr_id);

    return status;
}

herr_t save_state (state *s,
                   hid_t  file_id)
{
     hid_t group_id;
     herr_t status;
     int Nx_int, Ny_int, Nz_int;

     /* Make Group from simulation time `t` */

     char groupname[50];
     char step_str[10];
     sprintf(step_str, "%d", s->step);
     sprintf(groupname, "%0*d%s", LEN-(int)strlen(step_str), 0, step_str);

     group_id = H5Gcreate (file_id,
                           groupname,
                           H5P_DEFAULT,
                           H5P_DEFAULT,
                           H5P_DEFAULT);

     /* Write state attributes */

     status = write_double_attribute ("eta",    group_id, &s->eta);
     status = write_double_attribute ("chi",    group_id, &s->chi);
     status = write_double_attribute ("w",    group_id, &s->w);
     status = write_double_attribute ("eps0", group_id, &s->eps0);
     status = write_double_attribute ("eps1", group_id, &s->eps1);
     status = write_double_attribute ("eps", group_id, &s->eps);
     status = write_double_attribute ("Wc",    group_id, &s->Wc);
     status = write_double_attribute ("kA",    group_id, &s->k_A);
     status = write_double_attribute ("sig_kA", group_id, &s->sig_kA);
     status = write_double_attribute ("H0", group_id, &s->H0);
     status = write_double_attribute ("gamma_A", group_id, &s->gamma_A);
     // status = write_double_attribute ("kB",    group_id, &s->k_B);
     // status = write_double_attribute ("sig_kB",    group_id, &s->sig_kB);
     status = write_double_attribute ("T",    group_id, &s->T);
     status = write_double_attribute ("T0",    group_id, &s->T0);
     status = write_double_attribute ("TM",    group_id, &s->TM);
     status = write_double_attribute ("Tc",    group_id, &s->Tc);
     status = write_double_attribute ("a_c",   group_id, &s->a_c);

     status = write_double_attribute ("M_n",     group_id, &s->M_n);
     status = write_double_attribute ("xi_n",    group_id, &s->xi_n);
     status = write_double_attribute ("M_c",     group_id, &s->M_c);
     status = write_double_attribute ("xi_c",    group_id, &s->xi_c);

     status = write_double_attribute ("n0",    group_id, &s->n0);

     status = write_double_attribute ("misorrientation_x", group_id, &s->misOrr_x);
     status = write_double_attribute ("misorrientation_y", group_id, &s->misOrr_y);
     status = write_double_attribute ("misorrientation_z", group_id, &s->misOrr_z);

     status = write_double_attribute ("dx",     group_id, &s->dx);
     status = write_double_attribute ("dt",     group_id, &s->dt);
     status = write_double_attribute ("Time",   group_id, &s->t);
     status = write_int_attribute ("Time Step", group_id, &s->step);

     Nx_int = (int)s->Nx;
     status = write_int_attribute ("Nx", group_id, &Nx_int);
     Ny_int = (int)s->Ny;
     status = write_int_attribute ("Ny", group_id, &Ny_int);
     Nz_int = (int)s->Nz;
     status = write_int_attribute ("Nz", group_id, &Nz_int);

     status = write_array_dataset ("Concentration", group_id, s->c, s);
     status = write_array_dataset ("Density", group_id, s->n, s);
     int SzLq_x_int = (int)s->SzLq_x;
     status = write_int_attribute ("Ring Size x", group_id, &SzLq_x_int);
     int SzLq_y_int = (int)s->SzLq_y;
     status = write_int_attribute ("Ring Size y", group_id, &SzLq_y_int);
     int SzLq_z_int = (int)s->SzLq_z;
     status = write_int_attribute ("Ring Size z", group_id, &SzLq_z_int);

     status = H5Gclose (group_id);

     return status;
}

herr_t save_state_verbose (state *s,
                   hid_t  file_id)
{
     hid_t group_id;
     herr_t status;
     int Nx_int, Ny_int, Nz_int;

     /* Make Group from simulation time `t` */

     char groupname[50];
     char step_str[10];
     sprintf(step_str, "%d", s->step);
     sprintf(groupname, "%0*d%s", LEN-(int)strlen(step_str), 0, step_str);

     group_id = H5Gcreate (file_id,
                           groupname,
                           H5P_DEFAULT,
                           H5P_DEFAULT,
                           H5P_DEFAULT);

     /* Write state attributes */

     status = write_double_attribute ("eta",    group_id, &s->eta);
     status = write_double_attribute ("chi",    group_id, &s->chi);
     status = write_double_attribute ("w",    group_id, &s->w);
     status = write_double_attribute ("eps0", group_id, &s->eps0);
     status = write_double_attribute ("eps1", group_id, &s->eps1);
     status = write_double_attribute ("eps", group_id, &s->eps);
     status = write_double_attribute ("Wc",    group_id, &s->Wc);
     status = write_double_attribute ("kA",    group_id, &s->k_A);
     status = write_double_attribute ("sig_kA", group_id, &s->sig_kA);
     status = write_double_attribute ("H0", group_id, &s->H0);
     status = write_double_attribute ("gamma_A", group_id, &s->gamma_A);
     // status = write_double_attribute ("kB",    group_id, &s->k_B);
     // status = write_double_attribute ("sig_kB",    group_id, &s->sig_kB);
     status = write_double_attribute ("T",    group_id, &s->T);
     status = write_double_attribute ("T0",    group_id, &s->T0);
     status = write_double_attribute ("TM",    group_id, &s->TM);
     status = write_double_attribute ("Tc",    group_id, &s->Tc);
     status = write_double_attribute ("a_c",   group_id, &s->a_c);

     status = write_double_attribute ("M_n",     group_id, &s->M_n);
     status = write_double_attribute ("xi_n",    group_id, &s->xi_n);
     status = write_double_attribute ("M_c",     group_id, &s->M_c);
     status = write_double_attribute ("xi_c",    group_id, &s->xi_c);

     status = write_double_attribute ("n0",    group_id, &s->n0);

     status = write_double_attribute ("misorrientation_x", group_id, &s->misOrr_x);
     status = write_double_attribute ("misorrientation_y", group_id, &s->misOrr_y);
     status = write_double_attribute ("misorrientation_z", group_id, &s->misOrr_z);

     status = write_double_attribute ("dx",     group_id, &s->dx);
     status = write_double_attribute ("dt",     group_id, &s->dt);
     status = write_double_attribute ("Time",   group_id, &s->t);
     status = write_int_attribute ("Time Step", group_id, &s->step);

     Nx_int = (int)s->Nx;
     status = write_int_attribute ("Nx", group_id, &Nx_int);
     Ny_int = (int)s->Ny;
     status = write_int_attribute ("Ny", group_id, &Ny_int);
     Nz_int = (int)s->Nz;
     status = write_int_attribute ("Nz", group_id, &Nz_int);

     status = write_array_dataset ("Concentration", group_id, s->c, s);
     status = write_Re_ksize_array_dataset ("mag_k",   group_id, s->mag_k, s);
     status = write_Re_ksize_array_dataset ("C2_k",   group_id, s->C2_k, s);
     status = write_Rek_array_dataset ("Re{ck}",    group_id, s->ck, s);
     status = write_Imk_array_dataset ("Im{ck}",    group_id, s->ck, s);
     status = write_array_dataset ("Density", group_id, s->n, s);
     status = write_Rek_array_dataset ("Re{nk}",    group_id, s->nk, s);
     status = write_Imk_array_dataset ("Im{nk}",    group_id, s->nk, s);
     status = write_Rek_array_dataset ("Re{exp_A_k}",  group_id, s->expkA_k, s);
     status = write_Imk_array_dataset ("Im{exp_A_k}",  group_id, s->expkA_k, s);
     status = write_array_dataset ("exp_A",        group_id, s->expkA, s);
     // status = write_Rek_array_dataset ("Re{exp_B_k}",  group_id, s->expkB_k, s);
     // status = write_Imk_array_dataset ("Im{exp_B_k}",  group_id, s->expkB_k, s);
     // status = write_array_dataset ("exp_B",        group_id, s->expkB, s);
     status = write_array_dataset ("Corr_n*n",      group_id, s->Corr_n, s);
     status = write_array_dataset ("n*Corr_c*n",    group_id, s->Corr_c, s);
     status = write_Rek_array_dataset ("Re{NL_nk}", group_id, s->NL_nk, s);
     status = write_Imk_array_dataset ("Im{NL_nk}", group_id, s->NL_nk, s);
     status = write_Rek_array_dataset ("Re{NL_ck}", group_id, s->NL_ck, s);
     status = write_Imk_array_dataset ("Im{NL_ck}", group_id, s->NL_ck, s);
     status = write_array_dataset ("NL_n",        group_id, s->NL_n, s);
     status = write_array_dataset ("NL_c",        group_id, s->NL_c, s);

     int SzLq_x_int = (int)s->SzLq_x;
     status = write_int_attribute ("Ring Size x", group_id, &SzLq_x_int);
     int SzLq_y_int = (int)s->SzLq_y;
     status = write_int_attribute ("Ring Size y", group_id, &SzLq_y_int);
     int SzLq_z_int = (int)s->SzLq_z;
     status = write_int_attribute ("Ring Size z", group_id, &SzLq_z_int);

     status = H5Gclose (group_id);

     return status;
}

state* load_state (hid_t       file_id,
                   const char *datafile)
{
    state* s;
    ptrdiff_t Nx, Ny, Nz;
    int Nx_int, Ny_int, Nz_int;
    double dx0, dx, dt;
    hid_t group_id;
    herr_t status;

    group_id = H5Gopen2 (file_id, datafile, H5P_DEFAULT);

    read_int_attribute ("Nx", group_id, &Nx_int);
    read_int_attribute ("Ny", group_id, &Ny_int);
    read_int_attribute ("Nz", group_id, &Nz_int);
    // read_double_attribute ("dx0", group_id, &dx0);
    read_double_attribute ("dx", group_id, &dx);
    read_double_attribute ("dt", group_id, &dt);

    Nx = (ptrdiff_t)Nx_int;
    Ny = (ptrdiff_t)Ny_int;
    Nz = (ptrdiff_t)Nz_int;

    s = create_state (Nx, Ny, Nz, dx, dt,
                      0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0);
    assert (s != NULL);

    /* Read state attributes */

    status = read_double_attribute ("eta",    group_id, &s->eta);
    status = read_double_attribute ("chi",    group_id, &s->chi);
    status = read_double_attribute ("w",    group_id, &s->w);
    status = read_double_attribute ("eps0", group_id, &s->eps0);
    status = read_double_attribute ("eps1", group_id, &s->eps1);
    status = read_double_attribute ("eps", group_id, &s->eps);
    status = read_double_attribute ("Wc",    group_id, &s->Wc);
    status = read_double_attribute ("kA",    group_id, &s->k_A);
    status = read_double_attribute ("sig_kA", group_id, &s->sig_kA);
    status = read_double_attribute ("H0", group_id, &s->H0);
    status = read_double_attribute ("gamma_A", group_id, &s->gamma_A);
    // status = read_double_attribute ("kB",    group_id, &s->k_B);
    // status = read_double_attribute ("sig_kB",    group_id, &s->sig_kB);
    status = read_double_attribute ("T",    group_id, &s->T);
    status = read_double_attribute ("T0",    group_id, &s->T0);
    status = read_double_attribute ("TM",    group_id, &s->TM);
    status = read_double_attribute ("Tc",    group_id, &s->Tc);
    status = read_double_attribute ("a_c",   group_id, &s->a_c);

    status = read_double_attribute ("M_n",     group_id, &s->M_n);
    status = read_double_attribute ("xi_n",    group_id, &s->xi_n);
    status = read_double_attribute ("M_c",     group_id, &s->M_c);
    status = read_double_attribute ("xi_c",    group_id, &s->xi_c);

    status = read_double_attribute ("n0",    group_id, &s->n0);

    status = read_double_attribute ("misorrientation_x", group_id, &s->misOrr_x);
    status = read_double_attribute ("misorrientation_y", group_id, &s->misOrr_y);
    status = read_double_attribute ("misorrientation_z", group_id, &s->misOrr_z);

    s->cosOrr_x = cos(0.5*s->misOrr_x);
    s->sinOrr_x = sin(0.5*s->misOrr_x);
    s->cosOrr_y = cos(0.5*s->misOrr_y);
    s->sinOrr_y = sin(0.5*s->misOrr_y);
    s->cosOrr_z = cos(0.5*s->misOrr_z);
    s->sinOrr_z = sin(0.5*s->misOrr_z);

    status = read_double_attribute ("dx",     group_id, &s->dx);
    status = read_double_attribute ("dt",     group_id, &s->dt);
    status = read_double_attribute ("Time",   group_id, &s->t);
    status = read_int_attribute ("Time Step", group_id, &s->step);

    status = read_array_dataset ("Concentration", group_id, s->c, s);
    status = read_array_dataset ("Density", group_id, s->n, s);
    status = read_int_attribute ("Ring Size x", group_id, &s->SzLq_x);
    status = read_int_attribute ("Ring Size y", group_id, &s->SzLq_y);
    status = read_int_attribute ("Ring Size z", group_id, &s->SzLq_z);

    status = H5Gclose (group_id);

    return s;
}

herr_t op_func (hid_t loc_id, const char *name, const H5L_info_t *info,
            void *operator_data)
{
    herr_t          status;
    H5O_info_t      infobuf;
    char string[50];
    int last_save;
    int saved_step;
    // void* test;
    // int* int_test;
    // int save_to_load;
    /*
     * Get type of the object and display its name and type.
     * The name of the object is passed to this function by
     * the Library.
     */
    status = H5Oget_info_by_name (loc_id, name, &infobuf, H5P_DEFAULT);
    switch (infobuf.type) {
        case H5O_TYPE_GROUP:
            last_save = *(int *)operator_data;
            snprintf(string, 50, "%i", last_save);
            mpi_print(string);
            // last_save = atoi(operator_data);
            snprintf(string, 50, "\tGroup: %s\n", name);
            mpi_print(string);
            saved_step = atoi(name);
            // test = &saved_step;
            snprintf(string, 50, "%i", saved_step);
            mpi_print(string);

            // save_to_load = saved_step;
            save_to_load = saved_step > save_to_load ? saved_step : save_to_load;
            // test = saved_step > last_save ? &name : operator_data;
            // int_test = (int *)test;
            // snprintf(string, 50, "%s", &int_test);
            // mpi_print(string);
            // operator_data = test;
            // operator_data = saved_step > (int)operator_data ? saved_step : operator_data;
            // snprintf(operator_data, 50, "%s", name);

            break;
        case H5O_TYPE_DATASET:
            snprintf(string, 50, "\tDataset: %s\n", name);
            mpi_print(string);
            // printf ("  Dataset: %s\n", name);
            break;
        case H5O_TYPE_NAMED_DATATYPE:
            snprintf(string, 50, "\tDatatype: %s\n", name);
            mpi_print(string);
            // printf ("  Datatype: %s\n", name);
            break;
        default:
            snprintf(string, 50, "\tUnknown: %s\n", name);
            mpi_print(string);
            // printf ( "  Unknown: %s\n", name);
    }

    return 0;
}

state* load_last_saved_state(hid_t      file_id)
{
  state* s;
  herr_t err;
  void* last_save;
  save_to_load = -1;
  last_save = &save_to_load;

  err = H5Literate (file_id, H5_INDEX_NAME, H5_ITER_NATIVE, NULL, op_func, last_save);

  save_to_load = *(int *)last_save;
  char string[50];
  snprintf(string, 50, "Datastep to load is %i", save_to_load);
  mpi_print(string);

  char groupname[50];
  char step_str[10];
  sprintf(step_str, "%d", save_to_load);
  sprintf(groupname, "%0*d%s", LEN-(int)strlen(step_str), 0, step_str);
  s = load_state(file_id, groupname);

  return s;
}

state* load_grain_boundary (const char *datafile1,
                            const char *datafile2)
{
  // hid_t file_id = io_init_from_file (datafile1);
  // state* s;
  // s = load_last_saved_state(file_id);
  // MPI_Barrier (MPI_COMM_WORLD);
  // io_finalize(file_id);

  // file_id = io_init_from_file (datafile2);
  // state* temp_s = load_last_saved_state(file_id);
  // MPI_Barrier (MPI_COMM_WORLD);
  // io_finalize(file_id);

  // double state_intp;
  // for (int i = 0; i < s->local_n0; i++)
  // {
  //     for (int j = 0; j < s->Ny; j++)
  //     {
  //         int ij = i*2*((s->Ny>>1) + 1) + j;
  //         state_intp = 1.0 / (1.0 + exp(j - (s->Ny>>1)));
  //         s->c[ij] *= state_intp;
  //         s->n[ij] *= state_intp;
  //         s->c[ij] += (1.0 - state_intp) * temp_s->c[ij];
  //         s->n[ij] += (1.0 - state_intp) * temp_s->n[ij];
  //     }
  // }
  // MPI_Barrier (MPI_COMM_WORLD);
  // destroy_state(temp_s);
  state* s;
  state* s2;
  hid_t file_id;
  file_id = io_init_from_file (datafile1);
  s = load_last_saved_state(file_id);
  io_finalize(file_id);
  file_id = io_init_from_file (datafile2);
  s2 = load_last_saved_state(file_id);
  io_finalize(file_id);
  double state_intp;
  int ijk;
  for (int i = 0; i < s->local_n0; i++)
  {
      for (int j = 0; j < s->Ny; j++)
      {
          for (int k = 0; k < ((s->Nz>>1)+1); k++)
          {
            ijk = i*2*((s->Ny>>1) + 1) + j;
            state_intp = 1.0 / (1.0 + exp(j - (s->Ny>>1)));
            s->c[ijk] *= state_intp;
            s->n[ijk] *= state_intp;
            s->c[ijk] += (1.0 - state_intp) * s2->c[ijk];
            s->n[ijk] += (1.0 - state_intp) * s2->n[ijk];
          }
      }
  }
  MPI_Barrier (MPI_COMM_WORLD);
  destroy_state (s2);

  return s;
}

state* new_state_from_file (const char *filename)
{
    FILE *ifp;
    char *mode = "r";
    state* s;
    char name[20];
    double value;
    int Nx, Ny, Nz;
    double dt, dx;
    double n0, c0;
    double temp_eps;

    ifp = fopen (filename, mode);
    assert (ifp != NULL);

    while (fscanf(ifp, "%s : %lf", name, &value) != EOF)
    {
        if (strcmp (name, "Nx") == 0)       Nx = (int)value;
        else if (strcmp(name, "Ny") == 0)   Ny = (int)value;
        else if (strcmp(name, "Nz") == 0)   Nz = (int)value;
        else if (strcmp(name, "dx") == 0)   dx = value;
        else if (strcmp(name, "dt") == 0)   dt = value;
    }

    s = create_state (Nx, Ny, Nz, dx, dt,
                      0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0);
    assert (s != NULL);
    rewind (ifp);

    while (fscanf(ifp, "%s : %lf", name, &value) != EOF)
    {
        if (strcmp (name, "eta") == 0)           s->eta = value;
        else if (strcmp (name, "chi") == 0)      s->chi = value;
        else if (strcmp (name, "w") == 0)        s->w = value;
        else if (strcmp (name, "eps0") == 0)      s->eps0 = value;
        else if (strcmp (name, "eps1") == 0)      s->eps1 = value;
        else if (strcmp (name, "Wc") == 0)       s->Wc = value;
        else if (strcmp (name, "kA") == 0)       s->k_A = value;
        else if (strcmp (name, "sig_kA") == 0)   s->sig_kA = value;
        else if (strcmp (name, "H0") == 0)       s->H0 = value;
        else if (strcmp (name, "gamma_A") == 0)  s->gamma_A = value;
        // else if (strcmp (name, "kB") == 0)       s->k_B = value;
        // else if (strcmp (name, "sig_kB") == 0)   s->sig_kB = value;
        else if (strcmp (name, "T") == 0)        s->T = value;
        else if (strcmp (name, "T0") == 0)       s->T0 = value;
        else if (strcmp (name, "TM") == 0)        s->TM = value;
        else if (strcmp (name, "a_c") == 0)      s->a_c = value;
        else if (strcmp (name, "Tc") == 0)       s->Tc = value;
        else if (strcmp (name, "M_n") == 0)       s->M_n = value;
        else if (strcmp (name, "xi_n") == 0)     s->xi_n = value;
        else if (strcmp (name, "M_c") == 0)       s->M_c = value;
        else if (strcmp (name, "xi_c") == 0)     s->xi_c = value;
        else if (strcmp (name, "misorrientation_x") == 0)    s->misOrr_x = value;
        else if (strcmp (name, "misorrientation_y") == 0)    s->misOrr_y = value;
        else if (strcmp (name, "misorrientation_z") == 0)    s->misOrr_z = value;
        else if (strcmp (name, "n0") == 0)       n0 = value;
        else if (strcmp (name, "c0") == 0)       c0 = value;
        else if (strcmp (name, "Ring_Size_x") == 0)   s->SzLq_x = value;
        else if (strcmp (name, "Ring_Size_y") == 0)   s->SzLq_y = value;
        else if (strcmp (name, "Ring_Size_z") == 0)   s->SzLq_z = value;
    }

    int ijk;
    for (int i = 0; i < s->local_n0; i++)
    {
        for (int j = 0; j < Ny; j++)
        {
          for (int k = 0; k < 2*((Nz>>1)+1); k++)
          {
            ijk = k + 2*((Nz>>1)+1)*(i*Ny + j);
            s->c[ijk] = c0;
            s->n[ijk] = n0;
          }
        }
    }
    // Calculate the enthalpy of mixing coefficient
    temp_eps = s->eps0 + s->eps1 * (s->T - s->Tc);
    s->eps = temp_eps;
    // s->eps = 20.0;
    s->step = 0;
    s->t = 0.0;
    s->n0 = n0;
    // Calculate the rotation matrix entries
    s->cosOrr_x = cos(0.5*s->misOrr_x);
    s->sinOrr_x = sin(0.5*s->misOrr_x);
    s->cosOrr_y = cos(0.5*s->misOrr_y);
    s->sinOrr_y = sin(0.5*s->misOrr_y);
    s->cosOrr_z = cos(0.5*s->misOrr_z);
    s->sinOrr_z = sin(0.5*s->misOrr_z);
    // Modify the dx so the box size avoids spectral leakage
    // Need to potentially rethink this for 3d...
    s->dx *= 2*PI/(s->k_A * s->cosOrr_x);
    s->dx *= 2*PI/(s->k_A * s->cosOrr_y);
    s->dx *= 2*PI/(s->k_A * s->cosOrr_z);

    return s;
}
