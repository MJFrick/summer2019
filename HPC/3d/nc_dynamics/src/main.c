/*
*      Simulation from Input file
*
* Usage:
*  >> mpiexec -np <procs> ./main <inputfile> <outputfile> <final_t> <save_every>
*/

// Standard libraries
#include <mpi.h>
#include <hdf5.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
// Local libraries
#include "bcc_nc_header.h"


#define PI 2.0*acos(0.0)

int main(int argc, char** argv)
{
  // define the file id and blank state
  hid_t file_id;
  state* s;

  // Convert inputs to numbers
  double t_stop = atof(argv[3]);
  double sv_step = atof(argv[4]);
  double next_sv = sv_step;

  char string[100];

  // load any fft wisdom that may exist
  load_wisdom(argc, argv);

  // load the state from file
  s = new_state_from_file(argv[1]);


  mpi_print("Loaded File");

  seed_slab_bicrystal_bcc2(s, 0, 0.0);
  // seed_slab_multicrystal_bcc(s, 0, 6, 0.4);
  // seed_slab_bcc_L(s, 0, 0.0);
  // seed_c_gradient(s, 0, 0.2);
  // seed_slab_state_bcc(s, 0.0, 3.0, 0, s->Nx>>1, s->Nx, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, 0.0, 0.0);


  mpi_print("Seeded State");
  // // initialize output file
  file_id = io_init_new_file(argv[2]);

  // relax off any jagged corners of boundaries
  Prep_Simulation(s);
  mpi_print("Smoothed jagged corners");

  // Save initial State
  save_state_verbose(s, file_id);
  // mpi_print("saved initial state");
  // save_state(s, file_id);

  // sanity check
  mpi_print("\nStarting Simulation.\n");

  // run
  while (s->t < t_stop)
  // for (int i = 0; i < 10; i++)
  {

    MPI_Barrier(MPI_COMM_WORLD);
    step(s);
    if (s->t >= next_sv)
    {

      snprintf(string, 100, "\tSaving State\tt = %g at timestep = %i", s->t, s->step);
      mpi_print(string);

      save_state_verbose(s, file_id);

      next_sv += sv_step;
    }

  }
  MPI_Barrier(MPI_COMM_WORLD);

  // Finish up
  mpi_print("Finishing up...");
  io_finalize(file_id);
  destroy_state(s);
  save_wisdom();

  return 0;
}
