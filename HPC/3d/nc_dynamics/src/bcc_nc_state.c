// Try making an MPI syntectic system, since that's the only thing I currently
// have working.

// Define dependencies
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <fftw3-mpi.h>
#include <gsl/gsl_rng.h>
#include <time.h>

#include <sys/types.h>
#include <unistd.h>

#include "bcc_nc_header.h"

// Create the state we will use in the simulations, accross the MPI nodes
state* create_state(int Nx, int Ny, int Nz, double dx, double dt,
                    double eta, double chi,
                    double w, double Wc, double T, double T0, double TM, double Tc,
                    double a_c, double eps0, double eps1,
                    double k_A, double sig_kA, double H0, double gamma_A,
                    double Mn, double xi_n, double Mc, double xi_c,
                    double t, int step, double n0, double c0,
                    double misOrr_x, double misOrr_y, double misOrr_z,
                    int SzLq_x, int SzLq_y, int SzLq_z)
{
  // Define the size of the local array on each processor
  ptrdiff_t local_alloc;

  // Attempt to allocate memory.
  state* s = malloc(sizeof(state));

  // Error check
  if (s == NULL) return NULL;

  // Determine local array sizes
  /* Transposing saves a step on each fft calculation, which will add up
   * quickly. As a result however, we need to save an additional two ptrdiff_t
   * type variables and change the for loop bounds.
   */
  local_alloc = fftw_mpi_local_size_3d_transposed(Nx,Ny,(Nz>>1)+1, MPI_COMM_WORLD,
                                        &s->local_n0, &s->local_0_start,
                                        &s->local_n1, &s->local_1_start);

  // Allocate arrays
  s->mag_k = fftw_alloc_real(local_alloc);    // k-space array but doesn't need to be complex
  s->C2_k = fftw_alloc_real(local_alloc);    // k-space array but doesn't need to be complex
  s->n = fftw_alloc_real(2*local_alloc);
  s->nk = fftw_alloc_complex(local_alloc);
  s->c = fftw_alloc_real(2*local_alloc);
  s->ck = fftw_alloc_complex(local_alloc);
  s->NL_n = fftw_alloc_real(2*local_alloc);
  s->NL_nk = fftw_alloc_complex(local_alloc);
  s->NL_c = fftw_alloc_real(2*local_alloc);
  s->NL_ck = fftw_alloc_complex(local_alloc);
  s->Corr_n = fftw_alloc_real(2*local_alloc);
  s->Corr_nk = fftw_alloc_complex(local_alloc);
  s->Corr_c = fftw_alloc_real(2*local_alloc);
  s->Corr_ck = fftw_alloc_complex(local_alloc);
  s->expkA = fftw_alloc_real(2*local_alloc);
  s->expkA_k = fftw_alloc_complex(local_alloc);
  // s->expkB = fftw_alloc_real(2*local_alloc);
  // s->expkB_k = fftw_alloc_complex(local_alloc);
  s->xi_n_k = fftw_alloc_complex(local_alloc);
  s->xi_c_k = fftw_alloc_complex(local_alloc);

  // Error check
  if(s->n == NULL ||
     s->nk == NULL ||
     s->c == NULL ||
     s->ck == NULL ||
     s->NL_n == NULL ||
     s->NL_nk == NULL ||
     s->NL_c == NULL ||
     s->NL_ck == NULL ||
     s->Corr_n == NULL ||
     s->Corr_nk == NULL ||
     s->Corr_c == NULL ||
     s->Corr_ck == NULL ||
     s->expkA == NULL ||
     s->expkA_k == NULL ||
     // s->expkB == NULL ||
     s->xi_n_k == NULL ||
     s->xi_c_k == NULL ||
     s->mag_k == NULL ||
     s->C2_k == NULL
     )
  {
    // Free up memory, interrupt
    free(s->mag_k);
    free(s->C2_k);
    free(s->n);
    free(s->nk);
    free(s->c);
    free(s->ck);
    free(s->NL_n);
    free(s->NL_nk);
    free(s->NL_c);
    free(s->NL_ck);
    free(s->Corr_n);
    free(s->Corr_nk);
    free(s->Corr_c);
    free(s->Corr_ck);
    free(s->expkA);
    free(s->expkA_k);
    // free(s->expkB);
    // free(s->expkB_k);
    free(s->xi_n_k);
    free(s->xi_c_k);
    return NULL;
  }

  // Define the fftw plans.
  // Only need one plan since n and c are the same size.
  /* Since we are using transposed arrays in the mpi, we need to tell this to
   * the plan. Apparently this is done with a | between flags, though I can't
   * seem to find that in the fftw documentation.
   * Note also that only the fourier compenent field is transposed, so it's a
   * transposed out plan for the r2c, and a transposed in plan for the c2r.
   *
   * From fftw documentation:
   * "on each process input data is normally stored as a local_n0 by n1 array in
   * row-major order, but for an FFTW_MPI_TRANSPOSED_IN plan the input data is
   * stored as n1 by local_n0 in row-major order. Similarly,
   * FFTW_MPI_TRANSPOSED_OUT means that the output is n0 by local_n1 instead of
   * local_n1 by n0."
   * "local_n0 and local_0_start give the size and starting index of the n0
   * dimension for the non-transposed data, as in the previous sections. For
   * transposed data (e.g. the output for FFTW_MPI_TRANSPOSED_OUT), local_n1
   * and local_1_start give the size and starting index of the n1 dimension,
   * which is the first dimension of the transposed data"
   *
   * So we have loop over i in 0:local_n1 and j in 0:Nx for the k-space fields
   * and i in 0:local_n0 and j in 0:Ny for the real space fields as usual.
   * Which also means we only need complicated ij parts in real space.
   * In k-space we have simply ij = i*N + j.
   */
  s->pfft = fftw_mpi_plan_dft_r2c_3d(Nx, Ny, Nz, s->n, s->nk, MPI_COMM_WORLD,
                                      FFTW_MEASURE | FFTW_MPI_TRANSPOSED_OUT);
  s->ipfft = fftw_mpi_plan_dft_c2r_3d(Nx, Ny, Nz, s->nk, s->n, MPI_COMM_WORLD,
                                      FFTW_MEASURE | FFTW_MPI_TRANSPOSED_IN);

  // Define liquid initial conditions and the bragg ring
  int ijk;

  for (int i = 0; i < s->local_n0; i++)
  {
    for (int j = 0; j < Ny; j++)
    {
      for (int k = 0; k < 2*((Nz>>1)+1); k++)
      {
        ijk = k + 2*((Nz>>1)+1)*(j + Ny*i);
        s->n[ijk] = n0;
        s->c[ijk] = c0;
      }
    }
  }

  s->Nx = Nx;
  s->Ny = Ny;
  s->Nz = Nz;
  s->dx = dx;
  s->dt = dt;
  s->eta = eta;
  s->chi = chi;
  s->w = w;
  s->Wc = Wc;
  s->k_A = k_A;
  s->sig_kA = sig_kA;
  s->T = T;
  s->TM = TM;
  s->T0 = T0;
  s->H0 = H0;
  s->gamma_A = gamma_A;
  s->Tc = Tc;
  s->eps0 = eps0;
  s->eps1 = eps1;
  s->a_c = a_c;
  s->eps = eps0 + eps1*(T-Tc);
  s->M_n = Mn;
  s->xi_n = xi_n;
  s->M_c = Mc;
  s->xi_c = xi_c;
  s->SzLq_x = SzLq_x;
  s->SzLq_y = SzLq_y;
  s->SzLq_z = SzLq_z;
  s->misOrr_x = misOrr_x;
  s->misOrr_y = misOrr_y;
  s->misOrr_z = misOrr_z;
  s->sinOrr_x = sin(0.5*misOrr_x);
  s->cosOrr_x = cos(0.5*misOrr_x);
  s->sinOrr_y = sin(0.5*misOrr_y);
  s->cosOrr_y = cos(0.5*misOrr_y);
  s->sinOrr_z = sin(0.5*misOrr_z);
  s->cosOrr_z = cos(0.5*misOrr_z);
  s->t = t;
  s->step = step;

  // Nathan style entropy
  s->rng = gsl_rng_alloc (gsl_rng_default);
  if (s->rng == NULL)
  {
    gsl_rng_free (s->rng);
    return NULL;
  }
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  gsl_rng_set (s->rng, (
        (unsigned long)time(NULL)
         + (unsigned long)clock()
         + (unsigned long)getpid()
         + (unsigned long)getppid()) * (rank + 1));

  // Barrier to avoid pipeline issues.
  MPI_Barrier(MPI_COMM_WORLD);

  return s;
}

void destroy_state(state* s)
{
  // Free memory
  if (s != NULL)
  {
    fftw_destroy_plan (s->pfft);
    fftw_destroy_plan (s->ipfft);
    free(s->mag_k);
    free(s->C2_k);
    free(s->n);
    free(s->nk);
    free(s->c);
    free(s->ck);
    free(s->NL_n);
    free(s->NL_nk);
    free(s->NL_c);
    free(s->NL_ck);
    free(s->Corr_n);
    free(s->Corr_nk);
    free(s->Corr_c);
    free(s->Corr_ck);
    free(s->expkA);
    free(s->expkA_k);
    // free(s->expkB);
    // free(s->expkB_k);
    free(s->xi_n_k);
    free(s->xi_c_k);
    free (s);
  }
}

void seed_state_bcc(state* s, double kl, double Amp, int posx, int posy, int posz, int r,
                double orr_x, double orr_y, double orr_z, double seed_c, double seed_n)
{
  // add a crystal state at the centre of the array.
  int I;
  int ijk;
  double x;
  double y;
  double z;
  double plv1[] = {1.0, 0.0, 0.0};
  double plv2[] = {0.0, 1.0, 0.0};
  double plv3[] = {0.0, 0.0, 1.0};
  double rot_x[3][3] = {{1.0, 0.0, 0.0}, {0.0, cos(orr_x), -sin(orr_x)}, {0.0, sin(orr_x), cos(orr_x)}};
  double rot_y[3][3] = {{cos(orr_y), 0.0, sin(orr_y)}, {0.0, 1.0, 0.0}, {-sin(orr_y), 0.0, cos(orr_y)}};
  double rot_z[3][3] = {{cos(orr_z), -sin(orr_z), 0.0}, {sin(orr_z), cos(orr_z), 0.0}, {0.0, 0.0, 1.0}};
  double v1[3];
  double v2[3];
  double v3[3];
  double frac = 1.0 / 12.0;
  double smoother;

  for (int i = 0; i < 3; i++)
  {
    v1[i] = rot_x[i][0]*plv1[0] + rot_x[i][1]*plv1[1] + rot_x[i][2]*plv1[2];
    v2[i] = rot_x[i][0]*plv2[0] + rot_x[i][1]*plv2[1] + rot_x[i][2]*plv2[2];
    v3[i] = rot_x[i][0]*plv3[0] + rot_x[i][1]*plv3[1] + rot_x[i][2]*plv3[2];
  }
  for (int i = 0; i < 3; i++)
  {
    v1[i] = rot_y[i][0]*v1[0] + rot_y[i][1]*v1[1] + rot_y[i][2]*v1[2];
    v2[i] = rot_y[i][0]*v2[0] + rot_y[i][1]*v2[1] + rot_y[i][2]*v2[2];
    v3[i] = rot_y[i][0]*v3[0] + rot_y[i][1]*v3[1] + rot_y[i][2]*v3[2];
  }
  for (int i = 0; i < 3; i++)
  {
    v1[i] = rot_z[i][0]*v1[0] + rot_z[i][1]*v1[1] + rot_z[i][2]*v1[2];
    v2[i] = rot_z[i][0]*v2[0] + rot_z[i][1]*v2[1] + rot_z[i][2]*v2[2];
    v3[i] = rot_z[i][0]*v3[0] + rot_z[i][1]*v3[1] + rot_z[i][2]*v3[2];
  }

  double r_curr;

  for (int i = 0; i < s->local_n0; i++)
  {
    I = i + s->local_0_start;
    for (int j = 0; j < s->Ny; j++)
    {
      for (int k = 0; k < 2*((s->Nz>>1)+1); k++)
      {
        if (k >= (posz-r) && k <= (posz-r) && j >= (posy-r) && j <= (posy+r) && I >= (posx-r) && I <= (posx+r) &&
            (pow(k-posz,2)+pow(I-posx,2)+pow(j-posy,2)) <= pow(r,2))
        {
          r_curr = sqrt(pow(I-posx, 2.0) + pow(j-posy, 2.0) + pow(k-posz, 2.0));
          smoother = 1.0 - 1.0/(1.0+exp(-0.1*(r_curr-r))) - 1.0/(1.0+exp(0.1*(r_curr+r)));
          // Need to define a crystal seed here
          x = s->dx*I;
          y = s->dx*j;
          z = s->dx*k;
          ijk = k + 2*((s->Nz>>1)+1)*(j + s->Ny*i);
          s->c[ijk] += smoother * seed_c;

          kl = s->k_A;

          if (!((j) < s->SzLq_y || (j) > (s->Ny - s->SzLq_y) ||
            (i + s->local_0_start) < s->SzLq_x || (i + s->local_0_start) > (s->Nx - s->SzLq_x)))
            {
              s->n[ijk] += smoother * frac*Amp * (cos(kl*(v1[0]*x + v1[1]*y + v1[2]*z))*cos(kl*(v2[0]*x + v2[1]*y + v2[2]*z)) +
                                            cos(kl*(v2[0]*x + v2[1]*y + v2[2]*z))*cos(kl*(v3[0]*x + v3[1]*y + v3[2]*z)) +
                                            cos(kl*(v1[0]*x + v1[1]*y + v1[2]*z))*cos(kl*(v3[0]*x + v3[1]*y + v3[2]*z)));
            }

        }
      }
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
  return;
}

void seed_slab_state_bcc(state* s, double kl, double Amp, int dim, int pos, int r,
                double orr_x, double orr_y, double orr_z, double seed_c, double seed_n)
{
  // add a crystal state at the centre of the array.
  int I;
  int ijk;
  double x;
  double y;
  double z;
  double plv1[] = {1.0, 0.0, 0.0};
  double plv2[] = {0.0, 1.0, 0.0};
  double plv3[] = {0.0, 0.0, 1.0};
  int slab_vec[3];
  double rot_x[3][3] = {{1.0, 0.0, 0.0}, {0.0, cos(orr_x), -sin(orr_x)}, {0.0, sin(orr_x), cos(orr_x)}};
  double rot_y[3][3] = {{cos(orr_y), 0.0, sin(orr_y)}, {0.0, 1.0, 0.0}, {-sin(orr_y), 0.0, cos(orr_y)}};
  double rot_z[3][3] = {{cos(orr_z), -sin(orr_z), 0.0}, {sin(orr_z), cos(orr_z), 0.0}, {0.0, 0.0, 1.0}};
  double v1x[3];
  double v2x[3];
  double v3x[3];
  double v1xy[3];
  double v2xy[3];
  double v3xy[3];
  double v1[3];
  double v2[3];
  double v3[3];
  double frac = 1.0 / 12.0;
  double smoother;

  for (int i = 0; i < 3; i++)
  {
    v1x[i] = rot_x[i][0]*plv1[0] + rot_x[i][1]*plv1[1] + rot_x[i][2]*plv1[2];
    v2x[i] = rot_x[i][0]*plv2[0] + rot_x[i][1]*plv2[1] + rot_x[i][2]*plv2[2];
    v3x[i] = rot_x[i][0]*plv3[0] + rot_x[i][1]*plv3[1] + rot_x[i][2]*plv3[2];
  }
  for (int i = 0; i < 3; i++)
  {
    v1xy[i] = rot_y[i][0]*v1x[0] + rot_y[i][1]*v1x[1] + rot_y[i][2]*v1x[2];
    v2xy[i] = rot_y[i][0]*v2x[0] + rot_y[i][1]*v2x[1] + rot_y[i][2]*v2x[2];
    v3xy[i] = rot_y[i][0]*v3x[0] + rot_y[i][1]*v3x[1] + rot_y[i][2]*v3x[2];
  }
  for (int i = 0; i < 3; i++)
  {
    v1[i] = rot_z[i][0]*v1xy[0] + rot_z[i][1]*v1xy[1] + rot_z[i][2]*v1xy[2];
    v2[i] = rot_z[i][0]*v2xy[0] + rot_z[i][1]*v2xy[1] + rot_z[i][2]*v2xy[2];
    v3[i] = rot_z[i][0]*v3xy[0] + rot_z[i][1]*v3xy[1] + rot_z[i][2]*v3xy[2];
  }

  for (int i = 0; i < s->local_n0; i++)
  {
    I = i + s->local_0_start;
    for (int j = 0; j < s->Ny; j++)
    {
      for (int k = 0; k < 2*((s->Nz>>1)+1); k++)
      {
        slab_vec[0] = I;
        slab_vec[1] = j;
        slab_vec[2] = k;
        if (
          slab_vec[dim] >= (pos-r) && slab_vec[dim] <= (pos+r)
        )
        {
          smoother = 1.0 - 1.0/(1.0+exp(-0.1*(slab_vec[dim]-pos-r))) - 1.0/(1.0+exp(0.1*(slab_vec[dim]-pos+r)));
          // Need to define a crystal seed here
          x = s->dx*I;
          y = s->dx*j;
          z = s->dx*k;
          ijk = k + 2*((s->Nz>>1)+1)*(j + s->Ny*i);
          s->c[ijk] += smoother * seed_c;

          kl = s->k_A;

          if (!((j) < s->SzLq_y || (j) > (s->Ny - s->SzLq_y) ||
            (i + s->local_0_start) < s->SzLq_x || (i + s->local_0_start) > (s->Nx - s->SzLq_x)))
            {
              s->n[ijk] += smoother *frac*Amp * (cos(kl*(v1[0]*x + v1[1]*y + v1[2]*z))*cos(kl*(v2[0]*x + v2[1]*y + v2[2]*z)) +
                                            cos(kl*(v2[0]*x + v2[1]*y + v2[2]*z))*cos(kl*(v3[0]*x + v3[1]*y + v3[2]*z)) +
                                            cos(kl*(v1[0]*x + v1[1]*y + v1[2]*z))*cos(kl*(v3[0]*x + v3[1]*y + v3[2]*z)));
            }

        }
      }
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
  return;
}

void seed_slab_bicrystal_bcc(state* s, int dim, double c_shift)
{
  // based on dim argument, grab the correct dimension size
  int dim_len;
  if (dim == 0)   dim_len = s->Nx;
  else if (dim == 1)   dim_len = s->Ny;
  else if (dim == 2)   dim_len = s->Nz;
  else my_error("Dimensionality outside of bounds");
  // seed the alpha first
  seed_slab_state_bcc(s, 0.0, 1.6, dim, dim_len>>1, dim_len, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, -c_shift, 0.0);
  // remove alpha from the centre
  seed_slab_state_bcc(s, 0.0, -1.6, dim, dim_len>>1, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, 0.0, 0.0);
  seed_slab_state_bcc(s, 0.0, 0.0, dim,  dim_len>>1, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, c_shift, 0.0);
  // seed the beta
  seed_slab_state_bcc(s, 0.0, 1.6, dim, dim_len>>1, dim_len>>2, -0.5*s->misOrr_x, -0.5*s->misOrr_y, -0.5*s->misOrr_z, c_shift, 0.0);
  return;
}

void seed_slab_bicrystal_bcc2(state* s, int dim, double c_shift)
{
  // based on dim argument, grab the correct dimension size
  int dim_len;
  if (dim == 0)   dim_len = s->Nx;
  else if (dim == 1)   dim_len = s->Ny;
  else if (dim == 2)   dim_len = s->Nz;
  else my_error("Dimensionality outside of bounds");
  // seed the alpha first
  seed_slab_state_bcc(s, 0.0, 1.6, dim, dim_len>>2, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, -c_shift, 0.0);
  // remove alpha from the centre
  // seed_slab_state_bcc(s, 0.0, -1.6, dim, dim_len>>1, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, 0.0, 0.0);
  // seed_slab_state_bcc(s, 0.0, 0.0, dim,  dim_len>>1, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, c_shift, 0.0);
  // seed the beta
  seed_slab_state_bcc(s, 0.0, 1.6, dim, 3*dim_len>>2, dim_len>>2, -0.5*s->misOrr_x, -0.5*s->misOrr_y, -0.5*s->misOrr_z, c_shift, 0.0);
  return;
}

void seed_slab_multicrystal_bcc(state* s, int dim, int number, double c_shift)
{
  // based on dim argument, grab the correct dimension size
  int dim_len;
  if (dim == 0)   dim_len = s->Nx;
  else if (dim == 1)   dim_len = s->Ny;
  else if (dim == 2)   dim_len = s->Nz;
  else my_error("Dimensionality outside of bounds");

  int plus_minus = -1;
  int step = dim_len / number;
  int rad = step >> 1;

  for (int i = 0; i < number; i++)
  {
    plus_minus *= -1;
    seed_slab_state_bcc(s, 0.0, 0.8, dim, i * step, rad, plus_minus*0.5*s->misOrr_x, plus_minus*0.5*s->misOrr_y, plus_minus*0.5*s->misOrr_z, plus_minus*c_shift, 0.0);
  }
  plus_minus *= -1;
  seed_slab_state_bcc(s, 0.0, 0.8, dim, dim_len, rad, plus_minus*0.5*s->misOrr_x, plus_minus*0.5*s->misOrr_y, plus_minus*0.5*s->misOrr_z, plus_minus*c_shift, 0.0);
  return;
}

void seed_slab_bcc_L(state* s, int dim, double c_shift)
{
  // based on dim argument, grab the correct dimension size
  int dim_len;
  if (dim == 0)   dim_len = s->Nx;
  else if (dim == 1)   dim_len = s->Ny;
  else if (dim == 2)   dim_len = s->Nz;
  else my_error("Dimensionality outside of bounds");

  seed_slab_state_bcc(s, 0.0, 0.8, dim, dim_len>>1, dim_len, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, -c_shift, 0.0);
  // remove alpha from the centre
  seed_slab_state_bcc(s, 0.0, -0.8, dim, 0, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, 0.0, 0.0);
  seed_slab_state_bcc(s, 0.0, 0.0, dim, 0, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, 2*c_shift, 0.0);
  seed_slab_state_bcc(s, 0.0, -0.8, dim, dim_len, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, 0.0, 0.0);
  seed_slab_state_bcc(s, 0.0, 0.0, dim, dim_len, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, 2*c_shift, 0.0);
  return;
}

void seed_c_gradient(state* s, int dim, double c_shift)
{
  int dim_len;
  if (dim == 0)   dim_len = s->Nx;
  else if (dim == 1)   dim_len = s->Ny;
  else if (dim == 2)   dim_len = s->Nz;
  else my_error("Dimensionality outside of bounds");

  seed_slab_state_bcc(s, 0.0, 0.0, dim, dim_len>>1, dim_len>>2, 0.5*s->misOrr_x, 0.5*s->misOrr_y, 0.5*s->misOrr_z, c_shift, 0.0);

  return;
}
