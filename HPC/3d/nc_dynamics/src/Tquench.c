/*
*      Simulation from Input file
*
* Usage:
*  >> mpiexec -np <procs> ./main <inputfile> <total_T_quench> <final_t> <save_every>
*/

// Standard libraries
#include <mpi.h>
#include <hdf5.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
// Local libraries
// #include "SqTri_Isobaric-Binary-XPFC2.h"
// #include "bccbcc_Isobaric-Binary-XPFC_TD_AmpP.h"
#include "bcc_nc_header.h"


#define PI 2.0*acos(0.0)

int main(int argc, char** argv)
{
  // define the file id and blank state
  hid_t file_id;
  state* s;

  // Convert inputs to numbers
  double tot_quench = atof(argv[2]);
  double t_stop = atof(argv[3]);
  double rate = tot_quench / t_stop;
  double sv_step = atof(argv[4]);
  double next_sv = sv_step;

  char string[100];

  // load any fft wisdom that may exist
  load_wisdom(argc, argv);

  // continue from an old file
  file_id = io_init_from_file (argv[1]);
  s = load_last_saved_state(file_id);
  // io_finalize(file_id);
  double t_start = s->t;
  t_stop += s->t;
  next_sv += s->t;

  mpi_print("Loaded File");
    // Calc initials
    Calc_mag_k(s);
    Calc_Density_transforms(s);

    // sanity check
    mpi_print("\nStarting Simulation.\n");

    // run
    while (s->t < t_stop)
    {
      Ramp_T(s, t_start, t_stop, rate);
      MPI_Barrier(MPI_COMM_WORLD);
      step(s);

      if (s->t >= next_sv)
      {
        snprintf(string, 100, "\tSaving State\tt = %g at timestep = %i", s->t, s->step);
        mpi_print(string);
        save_state_verbose(s, file_id);
        next_sv += sv_step;
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // Finish up
    mpi_print("Finishing up...");
    io_finalize(file_id);
    destroy_state(s);
    save_wisdom();

    return 0;
  }
