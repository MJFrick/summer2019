// Dependencies
#include <stdlib.h>
#include <math.h>
#include <fftw3-mpi.h>
#include <float.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>

#include "bcc_nc_header.h"

// Define pi.
#define pi 2.0*acos(0.0)
// Define 1/3 as it's used often enough...
#define third 1.0/3.0

// Make functions for calculating k^2, kx, ky, kz.
// Make k^2 a field this time around since it get's called a lot.
double k2(int i, int j, int k, int Nx, int Ny, int Nz, double dx)
{
  // note that since we use a transposed fft, in k space i is the real space
  // y direction, and j is the real space x direction
  double Ly, ky2, Lx, kx2, Lz, kz2;
  Ly = Ny * dx;
  ky2 = i < ((Ny >> 1) + 1) ? pow(2.0*pi*i/Ly, 2.0) : pow(2.0*pi*(Ny-i)/Ly, 2.0);
  Lx = Nx * dx;
  kx2 = j < ((Nx >> 1) + 1) ? pow(2.0*pi*j/Lx, 2.0) : pow(2.0*pi*(Nx-j)/Lx, 2.0);
  Lz = Nz * dx;
  kz2 = k < ((Nz >> 1) + 1) ? pow(2.0*pi*k/Lz, 2.0) : pow(2.0*pi*(Nz-k)/Lz, 2.0);

  // return the length of the k vector
  return sqrt(kx2 + ky2 + kz2);
}

double k_x(int j, int Nx, double dx)
{
  double Lx, kx;
  Lx = Nx * dx;
  kx = j > ((Nx >> 1) + 1) ? 2.0*pi*j/Lx : 2.0*pi*(j-Nx)/Lx;
  return kx;
}

double k_y(int i, int Ny, double dx)
{
  double Ly, ky;
  Ly = Ny * dx;
  ky = i > ((Ny >> 1) + 1) ? 2.0*pi*i/Ly : 2.0*pi*(i-Ny)/Ly;
  return ky;
}

double k_z(int k, int Nz, double dx)
{
  double Lz, kz;
  Lz = Nz * dx;
  kz = k > ((Nz >> 1) + 1) ? 2.0*pi*k/Lz : 2.0*pi*(k-Nz)/Lz;
  return kz;
}

void Calc_mag_k(state* s)
{
  int index;
  double mag_k;

  // k-space loop.
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx;  j++)
    {
      for (int k = 0; k < ((s->Nz>>1)+1); k++)
      {
        index = k + ((s->Nz>>1)+1)*(j + s->Nx*i);
        s->mag_k[index] = k2(s->local_1_start + i, j, k, s->Nx, s->Ny, s->Nz, s->dx);
        mag_k = s->mag_k[index];
        s->C2_k[index] = exp(-1.0*(mag_k - sqrt(2)*s->k_A) * (mag_k - sqrt(2)*s->k_A) / (2.0 * s->sig_kA * s->sig_kA));
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);
  return;
}

// Define the interpolation functions and their derivatives wrt c
double Intp(state* s, double c)
{
  return exp(s->T * s->a_c * (c - 0.5)*(c - 0.5) / s->TM);
}

double DcIntp(state* s, double c)
{
  double first, second, out;
  first  = (2.0 * s->a_c * s->T * (c-0.5) / s->TM);
  second = 2.0 * s->a_c * s->T / s->TM;
  out = (first*first + second) * Intp(s, c);
  return out;
}

// Fourier transform and scale densities
void Calc_Density_transforms(state* s)
{
  int index;
  double norm = 1.0 / (s->Nx * s->Ny * s->Nz);
  fftw_mpi_execute_dft_r2c(s->pfft, s->n, s->nk);
  fftw_mpi_execute_dft_r2c(s->pfft, s->c, s->ck);

  // k-space loop
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx; j++)
    {
      for (int k = 0; k < ((s->Nz >> 1) + 1); k++)
      {
        index = k + ((s->Nz >> 1) + 1) * (j + s->Nx*i);
        s->nk[index][0] *= norm;
        s->nk[index][1] *= norm;
        s->ck[index][0] *= norm;
        s->ck[index][1] *= norm;
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  return;
}

// Calculate correlation terms
void Calc_Corr(state* s)
{
  double expT = exp(-1.0 * s->T / s->TM);
  double mag_k;
  int index;

  // k-space loop
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx; j++)
    {
      for (int k = 0; k < ((s->Nz >> 1) + 1); k++)
      {
        index = k + ((s->Nz >> 1) + 1) * (j + s->Nx*i);

        s->expkA_k[index][0] = s->C2_k[index] * s->nk[index][0];
        s->expkA_k[index][1] = s->C2_k[index] * s->nk[index][1];
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  // transform back to real space
  fftw_mpi_execute_dft_c2r(s->ipfft, s->expkA_k, s->expkA);
  // fftw_mpi_execute_dft_c2r(s->ipfft, s->expkB_k, s->expkB);

  MPI_Barrier(MPI_COMM_WORLD);

  // real space loop
  for (int i = 0; i < s->local_n0; i++)
  {
    for (int j = 0; j < s->Ny; j++)
    {
      for (int k = 0; k < s->Nz; k++)
      {
        index = k + 2*((s->Nz >> 1) + 1) * (j + s->Ny*i);
        s->Corr_n[index] = expT * Intp(s, s->c[index]) * s->expkA[index]; // + IntpB(s->c[index]) * s->expkB[index]);
        s->Corr_c[index] = expT * s->n[index] * DcIntp(s, s->c[index]) * s->expkA[index]; // + DcIntpB(s->c[index]) * s->expkB[index]);
        // s->Corr_n[index] = 1.0 * s->expkA[index];
        // s->Corr_c[index] = 1.0 * (s->rho_A[index] + s->rho_B[index] - 1.0) * s->expkA[index];
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  return;
}

// Calculate the non-linear terms in the dynamics.
void Calc_NL(state* s)
{
  int index;
  double n, c;
  double nLandau, nMix, cMix;
  double norm = 1.0 / (s->Nx * s->Ny * s->Nz);
  double t = s->eta;
  double v = s->chi;

  // real space loop
  for (int i = 0; i < s->local_n0; i++)
  {
    for (int j = 0; j < s->Ny; j++)
    {
      for (int k = 0; k < s->Nz; k++)
      {
        index = k + 2*((s->Nz >> 1) + 1) * (j + s->Ny*i);
        n = s->n[index];
        c = s->c[index];

        nLandau = -0.5 * t * n * n + third * v * n * n * n;
        nMix = s->w * (c * log(2.0 * c) + (1.0 - c) * log(2.0*(1.0 - c)));

        s->NL_n[index] = nLandau + nMix - s->Corr_n[index];

        cMix = s->w*(n + 1.0)*log(c / (1.0 - c));

        s->NL_c[index] = cMix - 0.5 * s->Corr_c[index];

        if ((i + s->local_0_start) < s->SzLq_x || (i + s->local_0_start) > (s->Nx - s->SzLq_x))
        {
          s->NL_n[index] += 0.5*(s->n[index] - s->n0);
          s->NL_c[index] += 0.05*(s->c[index] - 0.5);
        }
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  // tranform to k-space
  fftw_mpi_execute_dft_r2c(s->pfft, s->NL_n, s->NL_nk);
  fftw_mpi_execute_dft_r2c(s->pfft, s->NL_c, s->NL_ck);

  MPI_Barrier(MPI_COMM_WORLD);

  // scale
  // k-space loop
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx; j++)
    {
      for (int k = 0; k < ((s->Nz >> 1) + 1); k++)
      {
        index = k + ((s->Nz >> 1) + 1) * (j + s->Nx*i);

        s->NL_nk[index][0] *= norm;
        s->NL_nk[index][1] *= norm;
        s->NL_ck[index][0] *= norm;
        s->NL_ck[index][1] *= norm;
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  return;
}

// Calculate the noise.
void Calc_Noise(state* s)
{
  // Calculates noise in Fourier space using the gnu scientific library
  double scale_A, scale_B;
  double kx, ky, kz;

  scale_A = sqrt(s->xi_n / (s->Nx * s->Ny * s->Nz * s->dx * s->dx * s->dx * s->dt));
  scale_B = sqrt(s->xi_c / (s->Nx * s->Ny * s->Nz * s->dx * s->dx * s->dx * s->dt));


  int index;
  // For now I'll just set this to zero...
  // k-space loop
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx; j++)
    {
      for (int k = 0; k < ((s->Nz >> 1) + 1); k++)
      {
        index = k + ((s->Nz >> 1) + 1) * (j + s->Nx*i);
        kx = k_x(j, s->Nx, s->dx);
        ky = k_y(i + s->local_1_start, s->Ny, s->dx);
        kz = k_z(k, s->Nz, s->dx);
        // Come back to this later. Not strictly necessary yet.
        // s->xi_n_k[index][0] = scale_A *( kx * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + ky * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + kz * gsl_ran_gaussian_ziggurat(s->rng, 1.0));
        // s->xi_n_k[index][1] = scale_A *( kx * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + ky * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + kz * gsl_ran_gaussian_ziggurat(s->rng, 1.0));
        // s->xi_c_k[index][0] = scale_B *( kx * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + ky * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + kz * gsl_ran_gaussian_ziggurat(s->rng, 1.0));
        // s->xi_c_k[index][1] = scale_B *( kx * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + ky * gsl_ran_gaussian_ziggurat(s->rng, 1.0) + kz * gsl_ran_gaussian_ziggurat(s->rng, 1.0));
        s->xi_n_k[index][0] = 0.0;
        s->xi_n_k[index][1] = 0.0;
        s->xi_c_k[index][0] = 0.0;
        s->xi_c_k[index][1] = 0.0;
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  return;
}

void step(state* s)
{
  int index;
  double factor_n, factor_c_1, factor_c_2;
  double k_mag2, Lambda_n, Lambda_c, Q_n, L_n, P_c, Q_c, L_c;
  // First we transform and scale
  Calc_Density_transforms(s);

  // And the correlations
  Calc_Corr(s);
  // And the non-linear parts of the dynamics
  Calc_NL(s);

  // Calculate Noise
  Calc_Noise(s);


  factor_n = -s->T / s->T0 * s->M_n;
  factor_c_1 = -s->T / s->T0 * s->M_c * s->w * s->eps;
  factor_c_2 = -s->T / s->T0 * s->M_c * s->Wc;

  // Next we step the system
  // k-space loop
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx; j++)
    {
      for (int k = 0; k < ((s->Nz >> 1) + 1); k++)
      {
        index = k + ((s->Nz >> 1) + 1) * (j + s->Nx*i);
        k_mag2 = s->mag_k[index]*s->mag_k[index];

        Lambda_n = factor_n * k_mag2;
        Lambda_c = k_mag2 * (factor_c_1 + k_mag2 * factor_c_2);

        Q_n = s->dt * Lambda_n  / (1.0 - s->dt * Lambda_n);
        L_n = s->dt / (1.0 - s->dt * Lambda_n);

        P_c = s->dt * Lambda_c / (1.0 - s->dt * Lambda_c);
        Q_c = -1.0 * s->dt * s->M_c * s->T / s->T0 * k_mag2 / (1.0 - s->dt * Lambda_c);
        L_c = s->dt / (1.0 - s->dt * Lambda_c);

        // We adjust with a placeholder since both are dependent on both
        s->nk[index][0] += Q_n * (s->nk[index][0] + s->NL_nk[index][0])
                           + L_n * s->xi_n_k[index][0];
        s->nk[index][1] += Q_n * (s->nk[index][1] + s->NL_nk[index][1])
                           + L_n * s->xi_n_k[index][1];
        s->ck[index][0] += P_c * s->ck[index][0]
                           + Q_c * s->NL_ck[index][0]
                           + L_c * s->xi_c_k[index][0];
        s->ck[index][1] += P_c * s->ck[index][1]
                           + Q_c * s->NL_ck[index][1]
                           + L_c * s->xi_c_k[index][1];
      }
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  // Transform back to real space
  fftw_mpi_execute_dft_c2r(s->ipfft, s->nk, s->n);
  fftw_mpi_execute_dft_c2r(s->ipfft, s->ck, s->c);

  MPI_Barrier(MPI_COMM_WORLD);

  // update time in the state
  s->t += s->dt;
  s->step += 1;

  MPI_Barrier (MPI_COMM_WORLD);

  return;
}

void QD_step(state* s)
{
  int index;
  double factor_n, factor_c_1, factor_c_2;
  double k_mag2, Lambda_n, Lambda_c, Q_n, L_n, P_c, Q_c, L_c;
  // First we transform and scale
  Calc_Density_transforms(s);

  // And the correlations
  Calc_Corr(s);
  // And the non-linear parts of the dynamics
  Calc_NL(s);

  // Calculate Noise
  Calc_Noise(s);


  factor_n = -s->T / s->T0 * s->M_n;
  factor_c_1 = -s->T / s->T0 * s->M_c * s->w * s->eps;
  factor_c_2 = -s->T / s->T0 * s->M_c * s->Wc;

  // Next we step the system
  // k-space loop
  for (int i = 0; i < s->local_n1; i++)
  {
    for (int j = 0; j < s->Nx; j++)
    {
      for (int k = 0; k < ((s->Nz >> 1) + 1); k++)
      {
        index = k + ((s->Nz >> 1) + 1) * (j + s->Nx*i);
        k_mag2 = s->mag_k[index]*s->mag_k[index];

        Lambda_n = factor_n * k_mag2;
        Lambda_c = k_mag2 * (factor_c_1 + k_mag2 * factor_c_2);

        Q_n = s->dt * Lambda_n  / (1.0 - s->dt * Lambda_n);
        L_n = s->dt / (1.0 - s->dt * Lambda_n);

        P_c = s->dt * Lambda_c / (1.0 - s->dt * Lambda_c);
        Q_c = -1.0 * s->dt * s->M_c * s->T / s->T0 * k_mag2 / (1.0 - s->dt * Lambda_c);
        L_c = s->dt / (1.0 - s->dt * Lambda_c);

        // We adjust with a placeholder since both are dependent on both
        s->nk[index][0] += Q_n * (s->nk[index][0] + s->NL_nk[index][0])
                           + L_n * s->xi_n_k[index][0];
        s->nk[index][1] += Q_n * (s->nk[index][1] + s->NL_nk[index][1])
                           + L_n * s->xi_n_k[index][1];
        s->ck[index][0] += P_c * s->ck[index][0]
                           + Q_c * s->NL_ck[index][0]
                           + L_c * s->xi_c_k[index][0];
        s->ck[index][1] += P_c * s->ck[index][1]
                           + Q_c * s->NL_ck[index][1]
                           + L_c * s->xi_c_k[index][1];
      }
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  // Transform back to real space
  fftw_mpi_execute_dft_c2r(s->ipfft, s->nk, s->n);
  fftw_mpi_execute_dft_c2r(s->ipfft, s->ck, s->c);

  MPI_Barrier(MPI_COMM_WORLD);

  // update time in the state
  // s->t += s->dt;
  // s->step += 1;

  MPI_Barrier (MPI_COMM_WORLD);

  return;
}

void Prep_Simulation(state* s)
{
  mpi_print("\tStarting dirty relaxation.");
  double dt_holder = s->dt;

  Calc_mag_k(s);
  s->dt = 0.00001;
  // for (int i = 0; i < 1000; i++)
  // {
  //   QD_step(s);
  // }

  s->dt = dt_holder;

  MPI_Barrier (MPI_COMM_WORLD);
  return;
}


void Ramp_T(state* s, double when, double duration, double rate)
{
  if (s->t >= when && s->t <= (when + duration))
  {
    s->T += rate*s->dt;
  }

  // Calculate the enthalpy of mixing coefficient
  s->eps = s->eps0 + s->eps1 * (s->T - s->Tc);
  return;
}
