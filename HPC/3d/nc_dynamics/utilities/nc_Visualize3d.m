close all;
clear all;
clc;

% Define filename and get metadata
filename = "../data/nc_Test.h5";
file_info = h5info(filename);

% Grab number of saved timesteps and size of array
N_times = length(file_info.Groups);
Nx = h5readatt(filename, '/00000000', 'Nx');
Ny = h5readatt(filename, '/00000000', 'Ny');
Nz = h5readatt(filename, '/00000000', 'Nz');

% Scaling factor to generate a box to observe a smaller portion of the system
fcx = 1;
fcy = 1;
fcz = 1;
% Define the grid vectors
x = double(1:1:Nx/fcx); y = double(1:1:Ny/fcy); z = double(1:1:Nz/fcz);
% Generate the grid
% For some reason this is permuted weirdly. Return how i expect them to be.
[X,Y,Z] = meshgrid(y,x,z);


% Create pictures for each saved timestep
for i = 1:N_times
    time = file_info.Groups(i).Name;
%     rho_A = h5read(filename, strcat(time, '/rho_A'));
%     rho_B = h5read(filename, strcat(time, '/rho_B'));
    c = h5read(filename, strcat(time, '/Concentration'));
    n = h5read(filename, strcat(time, '/Density'));
    
    n = permute(n,[3,2,1]);
    n(:,1:5,1:5) = 0;
    c = permute(c,[3,2,1]);
    
    figure(1);
    clf;
    
    figure(1);
    isosurface(X,Y,Z,n,0.20,c);
    colormap(redblue)
    colorbar();
%     caxis([0,1]);
    axis off
    daspect([1 1 1]);
    
    pic_name = strcat(time(2:end), '.png');
    print(pic_name,'-dpng','-r600');
end