using PyPlot
using Interact
using Maxima
using Optim

f_ideal = m"n^2/2 - t/6*n^3 + v/12*n^4"

n_tri = m"n0 + 2*A[1]*n1 + 2*A[2]*n2"

f_ideal_tri = subst(n_tri, :n, f_ideal)

f_ideal_tri = subst(n_tri, :n, f_ideal)

a1 = m"4 * %pi / sqrt(3) * y";
a2 = m"4 * %pi / sqrt(3) * (sqrt(3)*x/2 + y/2)";
a3 = m"4 * %pi / sqrt(3) * (sqrt(3)*x/2 - y/2)";
b1 = m"4 * %pi * x";
b2 = m"4 * %pi * (x/2 + sqrt(3)*y/2)";
b3 = m"4 * %pi * (x/2 - sqrt(3)*y/2)";
n1 = m"cos(a1) + cos(a2) + cos(a3)";
n2 = m"cos(b1) + cos(b2) + cos(b3)";
n1 = subst(a1, :a1, n1);
n1 = subst(a2, :a2, n1);
n1 = subst(a3, :a3, n1);
n2 = subst(b1, :b1, n2);
n2 = subst(b2, :b2, n2);
n2 = subst(b3, :b3, n2);
f_ideal_tri = subst(n1, :n1, f_ideal_tri);
f_ideal_tri = subst(n2, :n2, f_ideal_tri);

F_ideal_tri = "integrate(integrate( $f_ideal_tri, x, 0, 2 ), y, 0, 3*sqrt(3)/4 )" |> MExpr
F_ideal_tri = ratsimp(F_ideal_tri)
F_ideal_tri = "($F_ideal_tri)/(3*sqrt(3)/2)" |> MExpr |> mcall
F_ideal_tri = ratsimp(F_ideal_tri)
F_ideal_tri = float(F_ideal_tri)
F_ideal_tri = expand(F_ideal_tri)

F_ideal_tri = "$(F_ideal_tri)" |> MExpr |> mcall
F_ideal_tri_expr = parse(F_ideal_tri)
@eval F_id_tri(t, v, n0, A) = $(F_ideal_tri_expr)

F_mix(n0, c0, c, ω, ϵ0, ϵ1, T, Tc) = ω*((n0 + 1)*(c*log(c/c0) + (1-c)*log((1-c)/(1-c0))) + 0.5*(ϵ0 + ϵ1*(T-Tc))*(c-c0)^2)

Intp(c) = 1 - 3*c^2 + 2*c^3

const T0 = 1.0
const kA = 4*π/sqrt(3)
const kB = 2*π

const σA = 0.8
const σB = 0.8

const K0 = 0.0
const σ0 = 0.2

C_0(k,T) = K0*exp(-T/T0)*exp(-k^2/(2*σ0^2))
C_tri_1(k,c,T) = Intp(c)*exp(-T/T0)*exp(-(k-kA)^2/(2*σA^2))
C_tri_2(k,c,T) = Intp(1-c)*exp(-T/T0)*exp(-(k-kB)^2/(2*σB^2))

C(k,c,T) = C_tri_1(k,c,T) + C_tri_2(k,c,T)

F_ex_tri(k,c,T,A) = -12*A[1]^2*C(k,c,T) - 12*A[2]^2*C(√(3)*k,c,T)

F_tri(k, t, v, A, ω, n0, c, c0, ϵ0, ϵ1, T, Tc) = F_id_tri(t, v, n0, A) + n0^2*C_0(0.0, T) + F_ex_tri(k, c, T, A)
F_liq(t, v, ω, n0, c, c0, ϵ0, ϵ1, T, Tc) = (n0^2*C_0(0.0, T) + F_id_tri(t, v, n0, [0.0, 0.0]))

function F_tri(t, v, ω, n0, c, c0, ϵ0, ϵ1, T, Tc)
    k_guess = Intp(c)*kA + Intp(1-c)*kB
    sol = optimize(a-> F_tri(a[1], t, v, [a[2] a[3]], ω, n0, c, c0, ϵ0, ϵ1, T, Tc), [k_guess, 0.1, 0.01])
    return sol.minimum
end

function F(t, v, ω, n0, c, c0, ϵ0, ϵ1, T, Tc)
    tri_F = F_tri(t, v, ω, n0, c, c0, ϵ0, ϵ1, T, Tc)
    liq_F = F_liq(t, v, ω, n0, c, c0, ϵ0, ϵ1, T, Tc)
    id_ex, phase = findmin([tri_F,liq_F])
    mix = F_mix(n0, c0, c, ω, ϵ0, ϵ1, T, Tc)
    return (T/T0 * (id_ex + mix))
end

const t = 1.4
const v = 1.0
const ϵ0 = -4.0
const ϵ1 = 10.0
const Tc = 1.4
const ω = 0.02
const n0 = 0.05
const c0 = 0.5

F(c,T) = F(t, v, ω, n0, c, c0, ϵ0, ϵ1, T, Tc)

function isconvex(i, j, k, arr)
    @assert i < j < k
    slope = (arr[k]-arr[i])/(k-i)
    val = arr[i] + slope*(j-i)
    return arr[j] < val
end

function isstartingpoint(i, arr)
    return !isconvex(i-1, i, i+1, arr)
end

function expandRegion(region::Tuple{Int, Int}, arr::Vector)
    left = region[1]
    right = region[2]
    # Expand left
    if left != 1 && !isconvex(left-1, left, right, arr)
        left -= 1
    end
    if right != length(arr) && !isconvex(left, right, right+1, arr)
        right += 1
    end
    return (left, right)
end

function mergeregions!(regions::Vector{Tuple{Int, Int}})
    i = 1
    while i < lastindex(regions)
        if regions[i][2] >= regions[i+1][1]
            regions[i] = (regions[i][1], regions[i+1][2])
            deleteat!(regions, i+1)
        end
        i += 1
    end
end

function commontangentNathan(arr)
    N = length(arr)
    convexregions = Tuple{Int, Int}[]

    # Find starting points
    for i in 2:(N-1)
        isstartingpoint(i, arr) ? push!(convexregions, (i-1, i+1)) : continue
    end
    mergeregions!(convexregions)

    oldregions = Tuple{Int, Int}[]

    while oldregions != convexregions
        oldregions = copy(convexregions)
        for i in eachindex(convexregions)
            convexregions[i] = expandRegion(convexregions[i], arr)
        end
        mergeregions!(convexregions)
    end
    return convexregions
end

function commontangentNathan(arr...)
    f = min(arr...)
    return commontangent(f)
end

function make_phase_diagram_Nathan(F, crnge, Trnge)
    T_vals = Float64[]
    c_vals = Float64[]

    c = collect(crnge)
    T = collect(Trnge)

    f = Array{Float64}(undef,length(c))

    for t in 1:length(T)
        for cc in 1:length(c)
            f[cc] = F(c[cc], T[t])
        end
        arr = commontangentNathan(f)
        for domain in arr
            for point in domain
                push!(T_vals, T[t])
                push!(c_vals, c[point])
            end
        end
    end
    return T_vals, c_vals
end

cvec = 0.01:0.01:0.99;
T_vec = 1.3:0.0025:1.6;
T_vals,c_vals = make_phase_diagram_Nathan(F, cvec, T_vec)
clf()
scatter(c_vals, T_vals)
xlim(0,1)

savefig("Eutectic.png")
