# Summer2019
Code for use by City University of Hong Kong summer students.

There are three subfolders, one of which contains julia code for creation of phase diagrams, one of which contains julia code for simple test runs, and one of which contains high performance code.
